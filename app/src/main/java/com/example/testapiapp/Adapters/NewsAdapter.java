package com.example.testapiapp.Adapters;

import android.media.Image;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.testapiapp.Model.News;
import com.example.testapiapp.R;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsPlaceholder> {

    private List<News> newsList;
    public SelectedNews selectedNews;

    public NewsAdapter(List<News> news, SelectedNews selectedNews) {
        this.newsList = news;
        this.selectedNews = selectedNews;
    }

    public static class NewsPlaceholder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView title;
        public ImageView image;
        public TextView date;
        private SelectedNews selectedNews;

        public NewsPlaceholder(View v, SelectedNews selectedNews) {
            super(v);
            title = v.findViewById(R.id.n_title);
            image = v.findViewById(R.id.n_image);
            date = v.findViewById(R.id.n_date);
            this.selectedNews = selectedNews;

            v.setOnClickListener(this);
        }

        public void bind(News news) {
            title.setText(news.getTitle());
            date.setText(getFormatedDate(news.getPublished()));
            //load image, if exists url
            if (news.getUrl() != "none") {
                Picasso.Builder builder = new Picasso.Builder(date.getContext());
                builder.listener(new Picasso.Listener() {
                    @Override
                    public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception) {
                        exception.printStackTrace();
                    }
                });
                builder.build().load(news.getImage()).into(image);
            }
        }

        @Override
        public void onClick(View v) {
            selectedNews.selectedNews(getAdapterPosition());
        }
    }

    @NonNull
    @Override
    public NewsAdapter.NewsPlaceholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.news_item, parent, false);
        return new NewsPlaceholder(view, selectedNews);
    }

    @Override
    public void onBindViewHolder(@NonNull NewsAdapter.NewsPlaceholder holder, int position) {
        holder.bind(newsList.get(position));
    }

    @Override
    public int getItemCount() {
        return newsList == null ? 0 : newsList.size();
    }

    public void setItems(Collection<News> tweets) {
        newsList.addAll(tweets);
        notifyDataSetChanged();
    }

    public void clearItems() {
        newsList.clear();
    }

    public static String getFormatedDate(String rawDate) {
        //Date formatting
        OffsetDateTime d = OffsetDateTime.parse(rawDate,
                DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss Z"));

        String formattedDate = d.format(DateTimeFormatter.ofPattern("MM-dd-yyyy"));

        return formattedDate;
    }

    public interface SelectedNews {
        void selectedNews(int position);
    }
}
