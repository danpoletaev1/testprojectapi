package com.example.testapiapp.API;

import com.example.testapiapp.Model.Model;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NewsAPI {

    @GET("latest-news")
    Call<Model> getNews(@Query("language") String language, @Query("apiKey") String api_key);

}
