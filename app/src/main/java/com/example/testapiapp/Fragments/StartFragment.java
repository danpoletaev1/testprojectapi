package com.example.testapiapp.Fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.testapiapp.NavigationHost;
import com.example.testapiapp.R;
import com.google.android.material.button.MaterialButton;

public class StartFragment extends Fragment {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_start, container, false);

        MaterialButton browseButton = view.findViewById(R.id.next_button);

        browseButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ((NavigationHost) getActivity()).navigateTo(new BrowseNewsFragment(), false);
            }
        });

        return view;
    }
}
