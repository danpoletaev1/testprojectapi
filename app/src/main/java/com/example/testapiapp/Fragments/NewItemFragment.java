package com.example.testapiapp.Fragments;

import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentResultListener;

import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.testapiapp.Model.News;
import com.example.testapiapp.NavigationHost;
import com.example.testapiapp.R;
import com.google.android.material.button.MaterialButton;
import com.squareup.picasso.Picasso;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

public class NewItemFragment extends Fragment {

    private ImageView itemImage;
    private TextView itemTitle;
    private TextView itemDescirption;
    private TextView itemUrl;
    private TextView itemAuthor;
    private TextView itemDate;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_item, container, false);

        initializeVariables(view);

        getParentFragmentManager().setFragmentResultListener("newObject", this, new FragmentResultListener() {
            @Override
            public void onFragmentResult(@NonNull String key, @NonNull Bundle bundle) {
                News newObject = bundle.getParcelable("newsItem");
                setData(newObject);
            }
        });

        return view;
    }

    private void initializeVariables(View view) {
        itemImage = view.findViewById(R.id.item_image);
        itemTitle = view.findViewById(R.id.item_title);
        itemDescirption = view.findViewById(R.id.item_description);
        itemUrl = view.findViewById(R.id.item_url);
        itemAuthor = view.findViewById(R.id.item_author);
        itemDate = view.findViewById(R.id.item_date);
    }

    private void setData(News newsObject) {
        //load Image
        if (newsObject.getUrl() != "none") {
            Picasso.Builder builder = new Picasso.Builder(getContext());
            builder.listener(new Picasso.Listener() {
                @Override
                public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception) {
                    exception.printStackTrace();
                }
            });
            builder.build().load(newsObject.getImage()).into(itemImage);
        }
        //Set text
        itemTitle.setText(newsObject.getTitle());
        itemDescirption.setText(newsObject.getDescritption());
        itemUrl.setText(newsObject.getUrl());
        itemAuthor.setText(newsObject.getAuthor());
        itemDate.setText(getFormatedDate(newsObject.getPublished()));
    }

    public static String getFormatedDate(String rawDate) {
        //Date formatting
        OffsetDateTime d = OffsetDateTime.parse(rawDate,
                DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss Z"));

        String formattedDate = d.format(DateTimeFormatter.ofPattern("MM-dd-yyyy"));

        return formattedDate;
    }
}
