package com.example.testapiapp.Fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.example.testapiapp.API.NewsAPI;
import com.example.testapiapp.Adapters.NewsAdapter;
import com.example.testapiapp.Model.Model;
import com.example.testapiapp.Model.News;
import com.example.testapiapp.NavigationHost;
import com.example.testapiapp.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class BrowseNewsFragment extends Fragment implements NewsAdapter.SelectedNews {
    private static String API_KEY = "eqTVDRBUMd2sonuEDT9wh5A9UKzeiyK1BqUtY5Rim7Iq3eGU";
    private static String LANGUAGE = "en";

    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private NewsAdapter mAdapter;
    private List<News> allNews;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_browse, container, false);

        allNews = new ArrayList<>();
        initializeRecyclerView(view);

        loadNewsFromApi();

        return view;
    }

    private void loadNewsFromApi() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.currentsapi.services/v1/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        NewsAPI jsonPlaceHolderApi = retrofit.create(NewsAPI.class);

        Call<Model> call = jsonPlaceHolderApi.getNews(LANGUAGE, API_KEY);

        call.enqueue(new Callback<Model>() {
            @Override
            public void onResponse(Call<Model> call, Response<Model> response) {
                if (!response.isSuccessful()) {
                    return;
                }
                Model model = response.body();
                allNews = model.getNewsList();
                mAdapter.setItems(allNews);
                //mAdapter.setItems(allNews);
                Log.println(Log.DEBUG, "Success", "Response successfull");
            }

            @Override
            public void onFailure(Call<Model> call, Throwable t) {
                Log.println(Log.DEBUG, "Failure", "Response failure");
            }
        });
    }

    private void initializeRecyclerView(View view) {
        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        // specify an adapter (see also next example)
        mAdapter = new NewsAdapter(allNews, this);
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void selectedNews(int position) {
        Bundle result = new Bundle();
        result.putParcelable("newsItem", allNews.get(position));
        getParentFragmentManager().setFragmentResult("newObject", result);
        ((NavigationHost) getActivity()).navigateTo(new NewItemFragment(), true);
    }
}
