package com.example.testapiapp.Model;

import java.util.List;

public class Model {
    private String status;

    private List<News> news;

    public String getStatus() {
        return status;
    }

    public List<News> getNewsList() {
        return news;
    }
}
