package com.example.testapiapp.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class News implements Parcelable {
    private String id;

    private String title;

    private String description;

    private String url;

    private String author;

    private String image;

    private String language;

    private String[] category;

    private String published;

    protected News(Parcel in) {
        id = in.readString();
        title = in.readString();
        description = in.readString();
        url = in.readString();
        author = in.readString();
        image = in.readString();
        language = in.readString();
        category = in.createStringArray();
        published = in.readString();
    }

    public static final Creator<News> CREATOR = new Creator<News>() {
        @Override
        public News createFromParcel(Parcel in) {
            return new News(in);
        }

        @Override
        public News[] newArray(int size) {
            return new News[size];
        }
    };

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescritption() {
        return description;
    }

    public String getUrl() {
        return url;
    }

    public String getAuthor() {
        return author;
    }

    public String getImage() {
        return image;
    }

    public String getLanguage() {
        return language;
    }

    public String[] getCategory() {
        return category;
    }

    public String getPublished() {
        return published;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(author);
        dest.writeString(title);
        dest.writeString(published);
        dest.writeString(description);
    }
}
